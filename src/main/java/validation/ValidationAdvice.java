package validation;

import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public class ValidationAdvice {

    @ResponseBody
    public ValidationErrors handleMethodArgumentNotValid(
            MethodArgumentNotValidException exception) {

        List<FieldError> errors = exception.getBindingResult().getFieldErrors();

        // create ValidationErrors object (container for individual errors).

        // add errors from errors list to ValidationErrors object.

        // return created object. Spring converts it to Json and returns it to the client.

        return null;
    }
}