package dao;

import model.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.util.List;

@Primary
public class PostHsqlDao implements PostDao {

    private JdbcTemplate template;

    @Override
    public Post save(Post post) {
        String sql = "insert into post (id, title, text) "
                   + "values (NEXT VALUE FOR seq1, ?, ?)";

        GeneratedKeyHolder holder = new GeneratedKeyHolder();

        template.update(conn -> { return null; }, holder);

        post.setId(holder.getKey().longValue());

        return post;
    }

    @Override
    public List<Post> findAll() {
        String sql = "select id, title, text from post";

        return template.query(sql, (rs, rowNum) -> { return null; });
    }

    @Override
    public void delete(Long id) {
        String sql = "delete from post where id = ?";

        // template.update(...
    }

    @Override
    public Post findById(Long id) {
        String sql = "select id, title, text "
                   + "from post where id = ?";

        // template.queryForObject(sql, new Object[] {id}, ...);

        return null;
    }

}
